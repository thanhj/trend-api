# Trendy API

## Activating and Deactivating the Virtualenv
```
Activating:
  $ source superlists/bin/activate
Or (source /usr/local/bin/virtualenvwrapper.sh)
  $ workon superlists

Deactivating:
  $ deactivate
```

## Project operations
```
Running the Django dev server
    $ python manage.py runserver
Running the functional tests
    $ python manage.py test functional_tests
Running the unit tests
    $ python manage.py test lists
Running all tests
    $ python manage.py test
```

## Make Migration Database Script when models changed
```
$ python manage.py makemigrations
```

## Apply migration DB
```
$ python manage.py migrate
```

## Deploy static resources (CSS,JS)
```
$ python manage.py collectstatic

```

## Using Gunicorn for PROD
```
$ cd to [source]
(source)$ ../virtualenv/bin/gunicorn superlists.wsgi:application
```

## Linux useful commands
```/etc/systemd/system/gunicorn-staging.superlists.thetrendi.net.service
[Unit]
Description=Gunicorn server for staging.superlists.thetrendi.net

[Service]
Restart=on-failure
User=ubuntu
WorkingDirectory=/home/ubuntu/sites/staging.superlists.thetrendi.net/source
ExecStart=/home/ubuntu/sites/staging.superlists.thetrendi.net/virtualenv/bin/gunicorn \
    --bind unix:/tmp/staging.superlists.thetrendi.net.socket \
    superlists.wsgi:application

[Install]
WantedBy=multi-user.target
```

```
# this command is necessary to tell Systemd to load our new config file 
$ sudo systemctl daemon-reload
# this command tells Systemd to always load our service on boot 
$ sudo systemctl enable gunicorn-staging.superlists.thetrendi.net 
# this command actually starts our service
$ sudo systemctl start gunicorn-staging.superlists.thetrendi.net
#Check the Systemd logs for using 
$ sudo journalctl -u gunicorn-staging.superlists.thetrendi.net
# You can ask Systemd to check the validity of your service configuration: 
$ systemd-analyze verify /path/to/my.service
```

## Remotely deploy by using fab
```
$ SITE_NAME=staging.superlists.thetrendi.net fab -i myaws.pem -H myec2 -u ubuntu deploy
```
```
Staging:
$ SITE_NAME=staging.superlists.thetrendi.net fab -i /Users/thanh/Singtel/aws/myaws.pem -H ec2-13-229-104-186.ap-southeast-1.compute.amazonaws.com -u ubuntu deploy
$STAGING_SERVER=http://staging.superlists.thetrendi.net python manage.py test functional_tests

PROD:
$ SITE_NAME=superlists.thetrendi.net fab -i /Users/thanh/Singtel/aws/myaws.pem -H ec2-18-136-107-147.ap-southeast-1.compute.amazonaws.com -u ubuntu deploy
$STAGING_SERVER=http://superlists.thetrendi.net python manage.py test functional_tests
```
