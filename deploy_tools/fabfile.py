from fabric.contrib.files import append, exists, sed
from fabric.api import env, local, run
import random
import os

REPO_URL = 'https://gitlab.com/thanhj/trend-api.git'

def deploy():
    site_name = os.environ.get('SITE_NAME')
    site_folder = f'/home/{env.user}/sites/{site_name}'
    source_folder = site_folder + '/source'
    _install_and_update_os()
    _create_directory_structure_if_necessary(site_folder)
    _get_latest_source(source_folder)
    _update_settings(source_folder, site_name)
    _update_virtualenv(source_folder)
    _update_static_files(source_folder)
    _update_database(source_folder)
    _update_and_run_services(source_folder, site_name)

def _install_and_update_os():
    run(f'sudo apt update')
    run(f'sudo apt --assume-yes install nginx git python3.6 python3-venv')

def _create_directory_structure_if_necessary(site_folder):
    for subfolder in ('database', 'static', 'virtualenv', 'source'):
        run(f'mkdir -p {site_folder}/{subfolder}')

def _get_latest_source(source_folder):
    if exists(source_folder + '/.git'):
        run(f'cd {source_folder} && git fetch')
    else:
        run(f'git clone {REPO_URL} {source_folder}')

    current_commit = local("git log -n 1 --format=%H", capture=True)
    run(f'cd {source_folder} && git reset --hard {current_commit}')

def _update_settings(source_folder, site_name):
    settings_path = source_folder + '/superlists/settings.py'
    sed(settings_path, "DEBUG = True", "DEBUG = False")
    sed(settings_path,
        'ALLOWED_HOSTS =.+$',
        f'ALLOWED_HOSTS = ["{site_name}"]'
    )
    secret_key_file = source_folder + '/superlists/secret_key.py'
    if not exists(secret_key_file):
        chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
        key = ''.join(random.SystemRandom().choice(chars) for _ in range(50))
        append(secret_key_file, f'SECRET_KEY = "{key}"')
    append(settings_path, '\nfrom .secret_key import SECRET_KEY')

def _update_virtualenv(source_folder):
    virtualenv_folder = source_folder + '/../virtualenv'
    if not exists(virtualenv_folder + '/bin/pip'):
        run(f'python3.6 -m venv {virtualenv_folder}')
    run(f'{virtualenv_folder}/bin/pip install -r {source_folder}/requirements.txt')

def _update_static_files(source_folder):
    run(
        f'cd {source_folder}'
        ' && ../virtualenv/bin/python manage.py collectstatic --noinput'
    )

def _update_database(source_folder):
    run(
        f'cd {source_folder}'
        ' && ../virtualenv/bin/python manage.py migrate --noinput'
    )

def _update_and_run_services(source_folder, site_name):
    run(
        f'cd {source_folder}'
        f' && sed "s/SITENAME/{site_name}/g" deploy_tools/nginx.template.conf | sudo tee /etc/nginx/sites-available/{site_name}'
    )

    if not exists(f'/etc/nginx/sites-enabled/{site_name}'):
        run(
            f'sudo ln -s ../sites-available/{site_name} /etc/nginx/sites-enabled/{site_name}'
        )

    # if not exists('/etc/nginx/sites-enabled/default'):
    #     run('sudo rm -rf /etc/nginx/sites-enabled/default')

    run(
        f'cd {source_folder}'
        f' && sed "s/SITENAME/{site_name}/g" deploy_tools/gunicorn-systemd.template.service | sudo tee /etc/systemd/system/gunicorn-{site_name}.service'
    )

    run(
        'sudo systemctl daemon-reload'
        ' && sudo systemctl reload nginx'
        f' && sudo systemctl enable gunicorn-{site_name}'
        f' && sudo systemctl restart gunicorn-{site_name}'
    )







